<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendaftaranMatkulTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaftaran_matkul', function (Blueprint $table) {
            $table->unsignedBigInteger('mahasiswa_id');
            $table->unsignedBigInteger('kelas_mata_kuliah_id');
            $table->timestamps();
        });

        Schema::table('pendaftaran_matkul', function (Blueprint $table) {
            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswa');
            $table->foreign('kelas_mata_kuliah_id')->references('id')->on('kelas_mata_kuliah');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendaftaran_matkul');
    }
}
