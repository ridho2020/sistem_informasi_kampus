<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKelasMataKuliahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('kelas_mata_kuliah', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('kelas_id');
            $table->unsignedBigInteger('mata_kuliah_id');
            $table->timestamps();
        });

        Schema::table('kelas_mata_kuliah', function (Blueprint $table) {
            $table->foreign('kelas_id')->references('id')->on('kelas');
            $table->foreign('mata_kuliah_id')->references('id')->on('mata_kuliah');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelas_mata_kuliah');
    }
}
