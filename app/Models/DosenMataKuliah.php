<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DosenMataKuliah extends Model
{
    use HasFactory;
    protected $table = 'dosen_mata_kuliah';
    protected $fillable = ['dosen_id', 'mata_kuliah_id'];
    public $timestamps = false;

    public function dosen() {
        return $this->belongsTo(Dosen::class);
    }

    public function mata_kuliah() {
        return $this->belongsTo(MataKuliah::class);
    }

}
