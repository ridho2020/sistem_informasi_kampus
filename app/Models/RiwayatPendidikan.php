<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RiwayatPendidikan extends Model
{
    use HasFactory;

    protected $table = 'riwayat_pendidikan';
    protected $primaryKey = 'id';

    protected $fillable = ['dosen_id','strata','jurusan','sekolah', 'tahun_mulai', 'tahun_selesai'];
    public $timestamps = false;

     public function dosen(){
      return $this->BelongsTo(Dosen::class,'dosen_id');
    }

}
