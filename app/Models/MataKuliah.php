<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MataKuliah extends Model
{
    use HasFactory;
    protected $table = 'mata_kuliah';
    protected $primaryKey = 'id';
    protected $fillable = ['nama','sks'];
    public $timestamps = false;

    public function dosen_mata_kuliah() {
        return $this->hasMany(DosenMataKuliah::class);
    }

    public function kelas_mata_kuliah() {
        return $this->hasMany(KelasMataKuliah::class);
    }

}
