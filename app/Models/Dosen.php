<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    use HasFactory;
    
    protected $table = 'dosen';
    protected $primaryKey = 'id';
    protected $fillable = ['nama','nip','gelar'];
    public $timestamps = false;

    public function riwayatPendidikan(){
    //setiap profil memiliki satu mahasiswa
    return $this->hasMany(RiwayatPendidikan::class);
  }

    public function dosen_mata_kuliah() {
        return $this->hasMany(DosenMataKuliah::class);
    }

}
