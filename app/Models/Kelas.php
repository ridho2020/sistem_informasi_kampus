<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    use HasFactory;
    protected $table = 'kelas';
    protected $primaryKey = 'id';
    protected $fillable = ['nama'];
    public $timestamps = false;

    public function kelas_mata_kuliah() {
        return $this->hasMany(KelasMataKuliah::class);
    }

}
